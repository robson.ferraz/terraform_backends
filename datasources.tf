data "aws_ami" "amazon_linux" {
    most_recent = true
    owners      = ["amazon"] # deve ser fornecido no formato lista entre "[]"

    filter {  # forma de filtrar por parâmetro
        name = "name"

        values = ["amzn-ami-hvm-*-x86_64-gp2"]
    }
}